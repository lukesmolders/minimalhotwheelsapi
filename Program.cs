using Microsoft.EntityFrameworkCore;

internal class Program
{
    private static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        // Add services to the container.
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();


        var connectionString = builder.Configuration.GetConnectionString("MariaDbConnectionString");

        var serverVersion = new MySqlServerVersion(new Version(8, 0, 27));

        builder.Services.AddDbContext<HotWheelsDb>(
            dbContextOptions =>
            {
                dbContextOptions
                .UseMySql(connectionString, serverVersion);

                if (builder.Environment.IsDevelopment())
                {
                    dbContextOptions
                    .LogTo(Console.WriteLine, LogLevel.Information)
                    .EnableSensitiveDataLogging()
                    .EnableDetailedErrors();
                }

            }
        );
        builder.Services.AddDatabaseDeveloperPageExceptionFilter();

        var app = builder.Build();

        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.UseHttpsRedirection();

        // Get all the cars
        app.MapGet("/hotwheels/all", async (HotWheelsDb db) =>
            await db.MiniaturCar.ToListAsync());

        // Create new hotwheel
        app.MapPost("/hotwheels", async (MiniaturCar miniaturCar, HotWheelsDb db) =>
        {
            db.MiniaturCar.Add(miniaturCar);
            await db.SaveChangesAsync();

            return Results.Created($"/hotwheels/{miniaturCar.Id}", miniaturCar);
        });

        // Delete hotwheel
        app.MapDelete("/hotwheels/{id}", async (int id, HotWheelsDb db) =>
        {
            if (await db.MiniaturCar.FindAsync(id) is MiniaturCar miniaturCar)
            {
                db.MiniaturCar.Remove(miniaturCar);
                await db.SaveChangesAsync();
                return Results.Ok(miniaturCar);
            }

            return Results.NotFound();
        });

        app.Run();
    }
}