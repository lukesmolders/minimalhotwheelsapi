using System.ComponentModel.DataAnnotations;

public class BaseTable
{
    [Key]
    public int Id { get; set; }
}