using Microsoft.EntityFrameworkCore;

public class HotWheelsDb : DbContext
{
    public HotWheelsDb(DbContextOptions<HotWheelsDb> options)
    : base(options)
    {
        this.Database.Migrate();
    }

    public DbSet<MiniaturCar> MiniaturCar => Set<MiniaturCar>();
}
